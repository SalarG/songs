import { combineReducers } from 'redux';

const songsReducer = () => {
  return [
    { title: 'Wish you were here', duration: '4:05' },
    { title: 'Hey You', duration: '4:30' },
    { title: 'Breathe', duration: '2:47' },
    { title: 'Pigs on the wind', duration: '2:50' }
  ];
};

const selectedSongReducer = (selectedSong = null, action) => {
  if (action.type === 'SONG_SELECTED') {
    return action.payload;
  }

  return selectedSong;
};

export default combineReducers({
  songs: songsReducer,
  selectedSong: selectedSongReducer
});